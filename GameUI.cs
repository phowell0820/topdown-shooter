﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour {

	RectTransform healthBar;
	float maxBarWidth;

	// Use this for initialization
	void Start () {

		GameObject health = transform.FindChild ("Health").gameObject;
		healthBar = health.GetComponent<RectTransform>();
		maxBarWidth = healthBar.rect.width;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void RenderHealth(float health) {
		float newHealth = maxBarWidth * health;
		healthBar.sizeDelta = new Vector2(newHealth, healthBar.sizeDelta.y);
	}
}
