using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	// Prefabs
	public GameObject Player;
	public GameObject Enemy;
	public GameObject Tile;
	public GameObject GameUI;

	// Instantiated prefabs
	GameObject player;
	GameObject enemy;
	GameObject tile;
	GameObject gameUI;

	// Wall Manager GameObject
	public GameObject WallManager;

	public int currentWave;
	int totalWaves;

	int enemyCount;

	// Use this for initialization
	void Start () {
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Confined;
		currentWave = 1;
		totalWaves = 1;
		
		gameUI = (GameObject)Instantiate(GameUI);
		
		BuildFloor(currentWave);
		WallManager.GetComponent<Walls>().SetupWalls(currentWave);

		player = (GameObject)Instantiate (Player, Vector3.zero, Quaternion.identity);
		player.GetComponent<PlayerCharacter>().gameManager = this;
	
		SpawnEnemies (currentWave);
	}

	void SpawnEnemies (int wave) {
		GameObject enemy;
		Vector3 spawnPos;
		Vector3[] patrol;
		enemyCount = 0;
		switch(wave) {
		// Last Man Standing
		case 1:
			// Patrols
			spawnPos = new Vector3(-11, -5.87f, 0);
			enemy = SetupEnemy ("Guy", spawnPos);
			patrol = new Vector3[] {	spawnPos, 
										new Vector3(-11, 5.09f, 0),
										new Vector3(10.25f, 5.09f, 0),
										new Vector3(10.25f, -5.87f, 0),
										spawnPos};
			enemy.GetComponent<Enemy>().SetPatrol(patrol);

			spawnPos = new Vector3(10.25f, 5.09f, 0);
			enemy = SetupEnemy ("Guy", spawnPos);
			patrol = new Vector3[] {	spawnPos, 
										new Vector3(10.25f, -5.87f, 0),
										new Vector3(-11, -5.87f, 0),
										new Vector3(-11f, 5.09f, 0),
										spawnPos};
			enemy.GetComponent<Enemy>().SetPatrol(patrol);

			enemy = SetupEnemy ("Guy", new Vector3(-9.12f, 3.16f, 0));
			patrol = new Vector3[] {	new Vector3 (-9.12f, 3.16f, 0), 
										new Vector3(-2.76f, 3.16f, 0),
										new Vector3(-2.76f, 1.73f, 0),
										new Vector3(3.16f, 1.73f, 0),
										new Vector3(3.16f, 3.16f, 0),
										new Vector3 (9, 3.16f, 0)};
			enemy.GetComponent<Enemy>().SetPatrol(patrol);
			
			// Stationary Guns
			enemy = SetupEnemy("Turret", new Vector3(-10, 2.45f, 0));
			enemy = SetupEnemy ("Turret", new Vector3(8.9f, -2.38f, 0));
			enemy = SetupEnemy ("Turret", new Vector3(-9.08f, -1.78f, 0));
			break;
		/*case 2:
			// Patrols
			enemy = SetupEnemy ("Guy", new Vector3(-11, -5.87f, 0));
			patrol = new Vector3[] {	new Vector3 (-11, -5.87f, 0), 
												new Vector3(-11, 5.09f, 0),
												new Vector3(10.25f, 5.09f, 0),
												new Vector3(10.25f, -5.87f, 0),
												new Vector3 (-11, -5.87f, 0)};
			enemy.GetComponent<Enemy>().SetPatrol(patrol);
			break;*/
		}
	}

	GameObject SetupEnemy (string type, Vector3 spawn) {
		GameObject enemy = (GameObject)Instantiate(Enemy, spawn, Quaternion.identity);
		enemy.tag = type;
		enemy.GetComponent<Enemy>().Setup();

		enemyCount ++;

		return enemy;
	}

	void BuildFloor(int wave) {
		Vector3 startPos = new Vector3(-12.25f, 6.6f, 9);
		float tileSize = 1.26f;

		int wMax = 20;
		int hMax = 12;

		WallManager.GetComponent<Walls>().SetBounds(startPos, tileSize, wMax, hMax);

		switch (wave) {
		case 1:
			Vector3 renderPos = startPos;
			for(int w=0; w<wMax; w++) {
				for(int h=0; h<hMax; h++){
					LoadFloor (renderPos, w, h, wMax-1, hMax-1);
					renderPos.y -= tileSize;
				}
				renderPos.x += tileSize;
				renderPos.y = startPos.y;
			}
			break;
		}
	}

	void LoadFloor(Vector3 renderPos, int width, int height, int wMax, int hMax) {
		string pos = "";
		Sprite ret = null;

		tile = (GameObject)Instantiate (Tile, renderPos, Quaternion.identity);
		tile.transform.parent = GameObject.Find ("Floor").transform;
		
		if (height == 0) {
			pos += "top";
		} else if (height == hMax) {
			pos += "bottom";
		}
		if (width == 0) {
			pos += "left";
		} else if (width == wMax) {
			pos += "right";
		}

		switch (pos) {
		case "top":
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/DungeonWall/Dungeon_Walls_02") as Sprite;
			break;
		case "bottom":
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/DungeonWall/Dungeon_Walls_02") as Sprite;
			break;
		case "topleft":
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/DungeonWall/Dungeon_Walls_03") as Sprite;
			break;
		case "topright":
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/DungeonWall/Dungeon_Walls_04") as Sprite;
			break;
		case "bottomleft":
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/DungeonWall/Dungeon_Walls_06") as Sprite;
			break;
		case "bottomright":
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/DungeonWall/Dungeon_Walls_05") as Sprite;
			break;
		case "left":
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/DungeonWall/Dungeon_Walls_01") as Sprite;
			break;
		case "right":
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/DungeonWall/Dungeon_Walls_01") as Sprite;
			break;
		default:
			ret = Resources.Load<Sprite> ("Imports/Tiles/ExpansionTiles/Dungeon Floor/Dungeon_Floor_1") as Sprite;
			break;
		}
		tile.GetComponent<SpriteRenderer>().sprite = ret;

		if (pos == "top" || pos == "left" || pos == "bottom" || pos == "right") {
			tile.AddComponent<BoxCollider2D>();
			tile.layer = LayerMask.NameToLayer ("Level");
		}
	}

	public void EnemyKilled() {
		enemyCount -= 1;
	}

	public void ResetGame() {
		Application.LoadLevel ("first");
	}
	
	// Update is called once per frame
	void Update () {
		if (enemyCount <= 0) {
			if (currentWave + 1 > totalWaves) {
				ResetGame ();
			} else {
				currentWave++;
				player.transform.position = Player.transform.position;
				SpawnEnemies (currentWave);
			}
		}
	}
}
 