﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {

	public GameObject target;

	// Use this for initialization
	void Start () {
		transform.position = Camera.main.WorldToViewportPoint(target.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
