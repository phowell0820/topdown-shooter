﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public class Stats {
		public int health;
		public int maxHealth;
		public string type;
		public float fireRate;
		public float bulletSpeed;
		public int damage;

		public float moveSpeed;

		public Vector3[] patrol;

		public void Set(string enemy) {

			switch(enemy) {
			case "Pyro":
				health = 50;
				maxHealth = health;
				break;
			case "Turret":
				health = 50;
				damage = 10;
				fireRate = .5f;
				bulletSpeed = 6;
				maxHealth = health;
				break;
			case "MovingTurret":
			case "Cop":
			case "Guy":
				health = 50;
				damage = 7;
				fireRate = .3f;
				bulletSpeed = 6;
				moveSpeed = .5f;
				maxHealth = health;
				break;
			}
		}
	}

	public Stats stats;

	// Controls enemy behavior, set with SetBehavior()
	public string behavior;

	bool firing;
	float fireTimer = 0;

	// helper var so we don't have to access the animator each time we want to check for shooting
	bool shooting;

	bool dead;
	//Sprite deadSprite;

	public GameObject Bullet;
	public GameObject Weapon;
	
	GameObject player;
	GameObject weapon;
	GameManager gameManager;

	Animator animator;
	AudioClip fireSound;

	// Pathing vars
	Vector3 destination;
	int destIndex;
	bool reverse;
	bool patrolLoop;
	int seen;

	// How long the player has been firing (used to calculate accuracy)
	public float sustainedTimer;
	
	// How accurate you will be depending on how sustained your fire is (will always be between .5 and 1)
	float spread;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();

		SetDefaultBehavior ();
		SetAudio();

		// Cache player object for later use.
		player = GameObject.Find ("Player");
	}

	void Update() {
		DeleteFinishedSounds();
	}

	public void Setup() {
		SetStats ();
		SetSprite ();
	}

	void SetAudio () {
		switch(gameObject.tag) {
		case "Guy":
			fireSound = Resources.Load<AudioClip> ("Imports/Laser_construction_kit/Short bursts/UL_Short_burst_116") as AudioClip;
			break;
		case "Turret":
			fireSound = Resources.Load<AudioClip> ("Imports/Laser_construction_kit/Short bursts/UL_Short_burst_116") as AudioClip;
			break;
		}
	}

	public void SetStats () {
		stats = new Stats();
		stats.Set(gameObject.tag);
	}

	public void SetSprite() {
		string spriteLoc = "";

		switch(gameObject.tag) {
		case "Pyro":
			spriteLoc = "Imports/animations/animations/DoctorPyro/pyro_stand_0001";
			break;
		case "Turret":
		case "MovingTurret":
			spriteLoc = "Imports/animations/animations/turrets/machinegun/machinegun_still";
			break;
		case "Cop":
			spriteLoc = "Imports/animations/animations/officer/officer_torso_2h";
			weapon = (GameObject)Instantiate(Weapon);
			weapon.transform.parent = transform;
			weapon.transform.localPosition = new Vector3(.06f, .3f, 0);
			break;
		case "Guy":
			animator = GetComponent<Animator>();
			animator.enabled = true;
			break;
		}
		GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(spriteLoc) as Sprite;
	}

	void SetDefaultBehavior() {
		switch(gameObject.tag) {
		case "Turret":
			behavior = "stationary";
			break;
		case "Guy":
			behavior = "patrol";
			break;
		}
	}

	public void SetBehavior(string newBehavior) {
		behavior = newBehavior;
	}

	void OnTriggerEnter2D(Collider2D other) {
		// If it's a player bullet
		if (other.gameObject.layer == LayerMask.NameToLayer ("PlayerProjectile")) {
			DeductHealth(other.gameObject.GetComponent<Bullet>().damage);
			Destroy (other.gameObject);
		}
	}

	void DeductHealth(int damage) {
		stats.health -= damage;
	}

	public void SetPatrol (Vector3[] patrol) {
		stats.patrol = patrol;
		destIndex = 1;
		destination = patrol[1];

		if (patrol[0] == patrol[patrol.Length - 1]) {
			patrolLoop = true;
		} else {
			patrolLoop = false;
		}
	}

	void FixedUpdate () {
		if (dead) {return;}
		if (stats.health <= 0) {
			KillEnemy();
			gameManager.EnemyKilled ();
		}
		if (HasLineOfSight(player)) {
			seen += 1;
			if (seen > 7) {
				FaceTarget(player.transform.position);
				Shoot (player);
			} else {
				IdleBehavior();
			}
		} else {
			seen = 0;
			IdleBehavior();
		} 
	}

	void KillEnemy() {
		// TODO: Temporary while I figure out how to handle death animations
		Destroy (gameObject);
		return;


		/*Destroy(animator);
		Destroy(GetComponent<Rigidbody2D>());
		Destroy (GetComponent<BoxCollider2D>());



		dead = true;
		transform.position = new Vector3(transform.position.x, transform.position.y, -1);*/
	}

	void IdleBehavior() {
		// Clear out sustained fire timer (for accuracy)
		sustainedTimer = 0;

		// Do behavior based on enemy stats
		switch(behavior) {
		case "patrol":
			if (animator != null && shooting) {
				animator.SetBool ("Shooting", false);
				shooting = false;
			}
			Patrol ();
			break;
		}
	}

	void Patrol() {
		if (destination == transform.position) {
			// Two types of patrol. looping and non-looping
			// Looping patrols have the same first and last patrol coordinate.
			if (patrolLoop == true) {
				if (stats.patrol.Length <= destIndex + 1) {
					destIndex = 1;
				} else {
					destIndex++;
				}
			// If reverse is true and destination index is above zero.  OR
			// If the length of patrol is less than our destination index
			} else if ((reverse && destIndex > 0) || stats.patrol.Length <= destIndex + 1) {
				reverse = true;
				destIndex--;
			} else {
				reverse = false;
				destIndex++;
			}
			destination = stats.patrol[destIndex];
		}
		FaceTarget (destination);
		// Start walking animation
		transform.position = Vector3.MoveTowards (transform.position, destination, stats.moveSpeed * .1f);
	}

	// Uses raycasting to determine if an enemy is in line of sight of an object
	bool HasLineOfSight(GameObject target) {

		int layerMask = (1 << target.layer) | (1 << LayerMask.NameToLayer ("Level"));

		RaycastHit2D hit = Physics2D.Linecast (transform.position, target.transform.position, layerMask);

		if (hit.collider != null && hit.transform.gameObject == target) {
			return true;
		} else {
			return false;
		}
	}

	void FaceTarget (Vector3 target) {
		// Configures a new rotation based on the target's transform position
		Quaternion newRotation = Quaternion.LookRotation(transform.position - target, Vector3.back);
		newRotation *= Quaternion.Euler (0, 0, 180);

		// Keeps the look rotation limited to the Z access (since we're in 2D)
		newRotation.x = 0;
		newRotation.y = 0;

		// "Slerps" the rotation so the rotation change isn't immediate.
		transform.rotation = Quaternion.Slerp (transform.rotation, newRotation, Time.deltaTime * 12);
	}

	public void Shoot (GameObject target) {
		if (animator != null && !shooting) {
			animator.SetBool ("Shooting", true);
			shooting = true;
		}
		// First, figure out how long the player has been firing for and how much their accuracy should be affected
		// Minimum accuracy at 3 seconds of sustained fire
		if (sustainedTimer >= 1) {
			spread = .04f;
		} else {
			spread = sustainedTimer / 24;
		}
		sustainedTimer += Time.deltaTime;
		// If your gun can shoot
		if (CanShoot ()) {
			// Create a bullet, give it speed and damage
			Quaternion bulletRotation;
			if (spread >= 1) {
				bulletRotation = transform.rotation * Quaternion.Euler (0, 0, 90);
			} else {
				bulletRotation = transform.rotation * Quaternion.Euler (0, 0, 90 * Random.Range (1-spread, 1+spread));
			}
			Vector3 firePos = transform.position;
			if (weapon != null) {
				firePos = weapon.transform.position;
			}
			GameObject bullet = (GameObject)Instantiate (Bullet, firePos, bulletRotation);
			bullet.GetComponent<SpriteRenderer>().color = new Color(1.0f, .5f, .5f);
			bullet.layer = LayerMask.NameToLayer ("Enemy Projectile");
			Bullet bulletScript = bullet.GetComponent<Bullet>();
			bulletScript.bulletSpeed = stats.bulletSpeed;
			bulletScript.damage = stats.damage;

			FireSound ();
		}
	}

	bool CanShoot () {
		// If this is the first time we've tried to shoot, we can shoot
		if (!firing) {
			firing = true;
			return true;
		} else {
			// Else, figure out when the last time we shot was
			fireTimer += Time.deltaTime;
			// if the total time between our last shot and now is greater than the fire rate, we can shoot
			if (fireTimer > stats.fireRate) {
				fireTimer = 0;
				return true;
			} else {
				return false;
			}
		}
	}

	void FireSound() {
		AudioSource gunAudio = gameObject.AddComponent<AudioSource>();
		gunAudio.volume = .4f;
		
		gunAudio.clip = fireSound;
		
		gunAudio.Play ();
	}
	
	void DeleteFinishedSounds() {
		AudioSource[] audio = GetComponents <AudioSource>();
		for (int i=0; i<audio.Length; i++) {
			if (!audio[i].isPlaying) {
				Destroy (audio[i]);
			}
		}
	}

	void onGUI() {
		Vector2 pos = Camera.main.WorldToScreenPoint(transform.position);
		GUI.Box (new Rect(pos.x, Screen.height - pos.y, 60, 20), stats.health + "/" + stats.maxHealth);
	}
}
