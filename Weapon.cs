using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	/**
	 * Weapon Stats
	 * 
	 * type - name for the weapon.  Ex. "Assault Rifle"
	 * fireRate - number of bullets fired per second.
	 * bulletSpeed - Velocity of the bullets fired.
	 * damage - amount of HP taken away from enemies when hit.
	 * accuracy - factor of how accurate the gun is.  Always between .5 and 2.
	 * 
	 */
	class stats {
		public string type;
		public float fireRate;
		public float bulletSpeed;
		public int damage;
		public int accuracy;
	}

	AudioClip fireSound;
	AudioClip bulletSound;

	public GameObject Bullet;
	stats weaponStats = new stats();

	// Time between each shot
	float fireTimer;

	// How long the player has been firing (used to calculate accuracy)
	public float sustainedTimer;

	// How accurate you will be depending on how sustained your fire is (will always be between .5 and 1)
	float spread;

	// If the player is currently firing or not
	public bool firing;

	void switchWeapon(string type) {
		switch (type) {
		case "Rifle":
			weaponStats.type = "Rifle";
			weaponStats.fireRate = .2f;
			weaponStats.bulletSpeed = 20;
			weaponStats.damage = 10;
			break;
		}
	}

	// Use this for initialization
	void Start () {
		transform.localScale = new Vector3(.7f, .7f, .7f);
		GrabSounds();
		switchWeapon ("Rifle");
	}

	void Update() {
		DeleteFinishedSounds ();
	}
	
	void GrabSounds() {
		fireSound = Resources.Load<AudioClip> ("Imports/MentalDreamsAssets/SoldierSoundsPack/fire") as AudioClip;
		bulletSound = Resources.Load<AudioClip> ("Imports/MentalDreamsAssets/SoldierSoundsPack/bulletshells04") as AudioClip;
	}

	public void Shoot () {
		// First, figure out how long the player has been firing for and how much their accuracy should be affected
		// Minimum accuracy at 3 seconds of sustained fire
		if (sustainedTimer >= 3) {
			spread = .25f;
		} else {
			spread = sustainedTimer / 12;
		}
		sustainedTimer += Time.deltaTime;
		// If your gun can shoot
		if (canShoot ()) {
			// Create a bullet, give it speed and damage
			Quaternion bulletRotation;
			if (spread >= 1) {
				bulletRotation = transform.rotation * Quaternion.Euler (0, 0, 90);
			} else {
				bulletRotation = transform.rotation * Quaternion.Euler (0, 0, 90 * Random.Range (1-spread, 1+spread));
			}
			GameObject bullet = (GameObject)Instantiate (Bullet, transform.position, bulletRotation);
			//bullet.GetComponent<SpriteRenderer>().color = Color.blue;
			Bullet bulletScript = bullet.GetComponent<Bullet>();
			bulletScript.bulletSpeed = weaponStats.bulletSpeed;
			bulletScript.damage = weaponStats.damage;

			// Play firing sound
			FireSound();
		}
	}

	void FireSound() {
		AudioSource gunAudio = gameObject.AddComponent<AudioSource>();
		AudioSource bulletAudio = gameObject.AddComponent<AudioSource>();

		gunAudio.clip = fireSound;
		bulletAudio.clip = bulletSound;

		gunAudio.Play ();
		bulletAudio.Play ();
	}

	void DeleteFinishedSounds() {
		AudioSource[] audio = GetComponents <AudioSource>();
		for (int i=0; i<audio.Length; i++) {
			if (!audio[i].isPlaying) {
				Destroy (audio[i]);
			}
		}
	}

	bool canShoot () {
		// If this is the first time we've tried to shoot, we can shoot
		if (!firing) {
			firing = true;
			return true;
		} else {
			// Else, figure out when the last time we shot was
			fireTimer += Time.deltaTime;
			// if the total time between our last shot and now is greater than the fire rate, we can shoot
			if (fireTimer > weaponStats.fireRate) {
				fireTimer = 0;
				return true;
			} else {
				return false;
			}
		}
	}
}
 