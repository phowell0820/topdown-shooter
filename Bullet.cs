﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float bulletSpeed;
	public Rigidbody2D rb;

	// How much damage this bullet inflicts
	public int damage;

	// Use this for initialization
	void Start () {
		bulletSpeed = 10;
		rb = GetComponent<Rigidbody2D>();
		Vector3 velocity = this.transform.rotation * Vector3.right * bulletSpeed;
		rb.velocity = velocity;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate() {

	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.layer == LayerMask.NameToLayer("Level")) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter (Collider other) {
		Debug.Log ("Triggered1");
		if (other.gameObject.layer == LayerMask.NameToLayer("Level")) {
			Destroy (gameObject);
		}
	}
}
