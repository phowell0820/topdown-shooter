﻿using UnityEngine;
using System.Collections;

public class PlayerCharacter : MonoBehaviour {

	Rigidbody2D controller;
	CircleCollider2D body;

	int health;
	int maxHealth;
	float maxSpeed;

	public GameObject Weapon;
	public GameObject Legs;
	public GameObject Crosshair;

	GameUI gameUI;

	GameObject weapon;
	GameObject legs;
	GameObject crosshair;

	Animator legsAnim;

	AudioSource hurtSound;

	Weapon weaponScript;

	public GameManager gameManager;
	
	// Use this for initialization
	void Start () {
		health = 100;
		maxHealth = health;

		gameObject.name = "Player";

		InitializeComponents();

		weapon = (GameObject)Instantiate (Weapon, new Vector3(.04f, .24f, 0), Quaternion.identity);
		legs = (GameObject)Instantiate (Legs, new Vector3(-.025f, .013f, 1), Quaternion.identity);
		crosshair = (GameObject)Instantiate (Crosshair, new Vector3(0, 2, 0), Quaternion.identity);

		weapon.transform.parent = transform;
		legs.transform.parent = transform;
		crosshair.transform.parent = transform;

		legsAnim = legs.GetComponent<Animator>();
		weaponScript = weapon.GetComponent<Weapon>();

		maxSpeed = 2.5f;

		gameUI = GameObject.Find ("UI(Clone)").GetComponent<GameUI>();
	}

	// Used for setting up private variables for gameobject components and giving some intial settings
	void InitializeComponents () {
		controller = GetComponent<Rigidbody2D>();
		controller.fixedAngle = true;
		controller.gravityScale = 0;
		
		body = GetComponent<CircleCollider2D>();
		body.offset = new Vector2(0, .07f);
		body.radius = .19f;

		hurtSound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		FollowMouse ();

		gameUI.RenderHealth((float)health / (float)maxHealth);

		if (health <= 0) {
			Destroy(gameObject);
			gameManager.ResetGame();
		}
		if (Input.GetAxis ("Fire1") == 0) {
			weaponScript.firing = false;
			weaponScript.sustainedTimer = 0;
		}
	}

	void FixedUpdate () {
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis("Vertical");

		controller.velocity = new Vector2(horizontal * maxSpeed, vertical * maxSpeed);

		if (controller.velocity.x != 0 || controller.velocity.y != 0) {
			legsAnim.SetBool ("moving", true);
		} else {
			legsAnim.SetBool ("moving", false);
		}

		if (Input.GetAxis ("Fire1") > 0) {
			weapon.GetComponent<Weapon>().Shoot ();
		}
	}

	// Called on Update.  Used to make the character sprite and aiming direction follow the player's mouse position.
	void FollowMouse () {
		// Ray of mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		// Raycast to determine live mouse position
		Physics.Raycast (ray, out hit);
		
		// Target and target direction
		Vector3 _target = hit.point;
		Vector3 targetDir = _target - transform.position;
		
		// New angle to point the sprite
		float angle = Mathf.Atan2 (targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90;
		transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
	}

	void OnTriggerEnter2D(Collider2D other) {
		// If it's an enemy bullet
		if (other.gameObject.layer == LayerMask.NameToLayer ("Enemy Projectile")) {
			//DeductHealth(other.gameObject.GetComponent<Bullet>().damage);
			DeductHealth(other.gameObject.GetComponent<Bullet>().damage);
			Destroy (other.gameObject);
		}
	}

	void DeductHealth (int amount) {
		health -= amount;
		hurtSound.Play ();
	}
}
