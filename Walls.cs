﻿using UnityEngine;
using System.Collections;

public class Walls : MonoBehaviour {

	public GameObject Wall;

	GameObject wall;

	// Min/Max wall placements
	float xMin, xMax, yMin, yMax;

	// Sprite dimensions
	float width, height;

	Vector3 drawPos;
	Vector3 verticalDrawPos;

	void Awake () {
		width = 1.13f;
		height = .28f;
	}

	public void SetBounds (Vector3 pos, float tileSize, int wMax, int hMax) {
		xMin = pos.x + (tileSize * 2);
		yMin = pos.y - (tileSize * 2);

		xMax = pos.x + (tileSize * (wMax - 2));
		yMax = pos.y - (tileSize * (hMax - 2));

		drawPos = new Vector3(xMin, yMin, 8);
		verticalDrawPos = new Vector3(xMin, yMin, 8);
	}

	public void SetupWalls (int wave) {
		//int rows = Mathf.Abs (Mathf.RoundToInt((yMax - yMin) / height));
		//int rowLength = Mathf.Abs (Mathf.RoundToInt ((xMax - xMin) / width));

		//Debug.Log (rows);
		//Debug.Log (rowLength);

		switch(wave) {
		case 1:
			RoomOne ();
			break;
		case 2:
			RoomTwo ();
			break;
		}
	}

	void RoomOne() {
		// Draw Horizontal
		for (int i=0; i<2; i++) {
			SpawnRow (9);
			IncrementDrawPos(3, "x");
			SpawnRow (8);
			NextRow (1);
		}

		NextRow (4);

		IncrementDrawPos(2, "x");
		SpawnRow (4);
		IncrementDrawPos(3, "x");
		SpawnRow (3);
		IncrementDrawPos (3, "x");
		SpawnRow (4);

		NextRow (5);

		SpawnRow (3);
		IncrementDrawPos(2, "x");
		SpawnRow (3);
		IncrementDrawPos(4, "x");
		SpawnRow (2);
		IncrementDrawPos(2, "x");
		SpawnRow (2);

		NextRow (5);

		SpawnRow (5);
		IncrementDrawPos(2, "x");
		SpawnRow (1);
		IncrementDrawPos(4, "x");
		SpawnRow (5);
		IncrementDrawPos(2, "x");
		SpawnRow(1);

		NextRow (5);

		IncrementDrawPos(3, "x");
		SpawnRow (5);
		IncrementDrawPos(4, "x");
		SpawnRow (6);

		NextRow (5);

		SpawnRow (4);
		IncrementDrawPos(3, "x");
		SpawnRow (6);
		IncrementDrawPos(2, "x");
		SpawnRow (3);

		NextRow(5);

		for (int i=0; i<2; i++) {
			SpawnRow (9);
			IncrementDrawPos(3, "x");
			SpawnRow (8);
			NextRow(1);
		}
	}

	void RoomTwo() {

	}

	void SpawnRow (int num) {
		for (int w=0; w<num; w++) {
			if (drawPos.x > xMax || drawPos.y < yMax) {
				continue;
			}
			wall = (GameObject)Instantiate (Wall, drawPos, Quaternion.Euler (0, 0, 90));
			wall.transform.parent = transform;
			IncrementDrawPos(1, "x");
		}
	}

	void SpawnVerticalRow (int num) {
		for (int w=0; w<num; w++) {
			if (verticalDrawPos.x > xMax || verticalDrawPos.y < yMax) {
				continue;
			}
			wall = (GameObject)Instantiate (Wall, verticalDrawPos, Quaternion.identity);
			wall.transform.parent = transform;
			IncrementVDrawPos(1, "y");
		}
	}

	void NextVRow(int rowsToSkip) {
		verticalDrawPos.y = yMin;
		//Debug.Log (rowsToSkip);
		//Debug.Log (height);
		IncrementVDrawPos(height * rowsToSkip, "x");
	}

	void NextRow(int rowsToSkip) {
		drawPos.x = xMin;
		//Debug.Log (rowsToSkip);
		//Debug.Log (height);
		IncrementDrawPos(height * rowsToSkip, "y");
	}

	void IncrementDrawPos(float incr, string dir) {
		if (dir == "x") {
			drawPos.x += incr;
		} else if (dir == "y") {
			drawPos.y -= incr;
		}
	}

	void IncrementVDrawPos(float incr, string dir) {
		if (dir == "x") {
			verticalDrawPos.x += incr;
		} else if (dir == "y") {
			verticalDrawPos.y -= incr;
		}
	}
}
